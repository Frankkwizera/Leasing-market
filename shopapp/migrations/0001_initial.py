# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-11 19:48
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category_name', models.CharField(help_text='Category name e.g: Dress,Pants', max_length=100, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_name', models.CharField(help_text='Name of the product', max_length=100)),
                ('title_image', models.FileField(help_text='Image to displayed first', upload_to='media')),
                ('time', models.DateTimeField(help_text='Time when the product is uploaded')),
                ('price', models.IntegerField(help_text='Price of the product')),
                ('quantity', models.IntegerField(help_text='quantity of this product available in your store')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shopapp.Category')),
            ],
        ),
        migrations.CreateModel(
            name='Sale',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(help_text='First Name', max_length=100)),
                ('second_name', models.CharField(help_text='Second Name', max_length=100)),
                ('quantity', models.IntegerField(help_text='Quantity requested')),
                ('time', models.DateTimeField(help_text='Time when Product requested')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shopapp.Product')),
            ],
        ),
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(help_text='First Name', max_length=100)),
                ('second_name', models.CharField(help_text='Second Name', max_length=100)),
                ('email', models.CharField(help_text='Email to receive on notification', max_length=100)),
                ('phone_number', models.IntegerField(help_text='Phone number')),
            ],
        ),
        migrations.AddField(
            model_name='product',
            name='seller',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shopapp.Seller'),
        ),
    ]
